<?php

/**
 * @file
 * Uc_targetbay.admin.inc
 *   Administrtive forms for uc TargetBay module.
 */

/**
 * Menu callback. Redirect the user to the right page.
 */
function targetbay_select_page() {

  if (variable_get('api_token', TRUE) === FALSE) {
    drupal_goto('admin/config/targetbay/settings');
  }
  if (variable_get('uc_targetbay_id', FALSE) === FALSE) {
    drupal_goto('admin/config/targetbay/register');
  }
  else {
    drupal_goto('admin/config/targetbay/connection');
  }
}

/**
 * Register form.
 */
function targetbay_register_form($form_state) {

  $library = libraries_detect('targetbay-php');
  // Check if the expected library path exists.
  if (!empty($library['installed'])) {

    $form['targetbay_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('Enter Your Full Name'),
      '#required' => TRUE,
    );

    $form['targetbay_email'] = array(
      '#type' => 'textfield',
      '#title' => t('Email Address'),
      '#description' => t('Enter the email address you wish to register with.'),
      '#required' => TRUE,
    );

    $form['targetbay_domain'] = array(
      '#type' => 'textfield',
      '#title' => t('Domain'),
      '#description' => t('Enter the main domain of the site. e.g. https://www.yourdomain.com (do include https://)'),
      '#required' => TRUE,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Register'),
    );

    $form['targetbay_markup'] = array(
      '#markup' => t('Already registered? <a href="@already_registered">Click here to enter your account information and get started!</a>',
            array('@already_registered' => url('admin/config/targetbay/settings'))),
    );

    $form['introduction'] = array(
      '#type' => 'markup',
      '#markup' => '<p>' .
      t('By clicking register you agree to the TargetBay Terms & Conditions.') . '</p>',
      '#weight' => 10,
    );

    // Set form redirect.
    $form['#redirect'] = 'admin/config/targetbay';

    // If we already have an account.
    if (variable_get('uc_targetbay_id', FALSE) !== FALSE) {
      $form['submit']['#disabled'] = TRUE;
      drupal_set_message(t('Your account is already configured. You can check your account settings <a href="@already_registered">on the settings page</a>.',
      array('@already_registered' => url('admin/config/targetbay/settings'))));
    }
  }

  // If no library.
  else {
    drupal_set_message(t('TargetBay PHP library is required.'), 'error');

    $form['help'] = array(
      '#type' => 'markup',
      '#markup' => '<p>' .
      t('
          See the <a href="@help_url">project page</a> for install instructions.
          <br/>
          Download the required libraries from
          <a href="@library_url">the TargetBay GitHub project</a>
          and place it in <code>@install_path</code>',
                array(
                  '@library_url' => $library['download url'],
                  '@help_url' => url('https://drupal.org/project/uc_targetbay'),
                  '@install_path' => 'sites/all/libraries/targetbay-php',
                )) . '</p>',
      '#weight' => 10,
    );
  }

  return $form;
}

/**
 * Implements hook_form_submit() for registration form.
 */
function targetbay_register_form_submit($form, &$form_state) {
  if ($targetbay = uc_targetbay_new()) {
    $data = array();
    $data['email'] = $form_state['values']['targetbay_email'];
    $data['display_name'] = $form_state['values']['targetbay_name'];
    $data['first_name'] = '';
    $data['last_name'] = '';
    $data['website_name'] = '';
    $data['password'] = '';
    $data['support_url'] = '';
    $data['callback_url'] = '';
    $data['url'] = $form_state['values']['targetbay_domain'];
    $data['install_step'] = 'done';

    $response = $targetbay->create_user($data);
  }
  else {
    watchdog('uc_targetbay', 'Could not load TargetBay library');
  }

  if (!empty($response)) {
    // If (isset($response->status->code) && $response->status->code == 200) {
    // variable_set('uc_targetbay_id', $response->response->app_key);
    // variable_set('uc_targetbay_secret', $response->response->secret);.
    variable_set('uc_targetbay_oauth', $response['oauth']);

    $message = t('Congratulations - Your sign up to TargetBay was successful!');
    drupal_set_message($message);
    drupal_goto('admin/config/targetbay/settings');
  }
  else {
    form_set_error('form', t('There was an error contacting the registration service'));
    $form_state['redirect'] = FALSE;
  }
}

/**
 * Settings form.
 */
function targetbay_settings_form($form_state) {
  $form['uc_targetbay_id'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('uc_targetbay_id', ''),
    '#description' => t('The TargetBay API Key'),
    '#required' => TRUE,
  );

  $form['uc_targetbay_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('API Secret'),
    '#default_value' => variable_get('uc_targetbay_secret', ''),
    '#description' => t('The TargetBay API Secret'),
    '#required' => TRUE,
  );

  $node_fields = field_info_instances('node');
  $product_field_options = array();
  foreach ($node_fields as $field_instances) {
    foreach ($field_instances as $field) {
      $field_info = field_info_field($field['field_name']);
      if ($field_info['type'] === 'uc_product_reference' || $field_info['type'] === 'entityreference') {
        $product_field_options[$field['field_name']] = $field['label'] . " (" . $field['field_name'] . ")";
      }
    }
  }

  $form['uc_targetbay_product_field'] = array(
    '#type' => 'select',
    '#title' => t('Product field'),
    '#description' => t('Select the field that will be used as the product on nodes for TargetBay.'),
    '#default_value' => variable_get('uc_targetbay_product_field', 'field_product'),
    '#options' => $product_field_options,
  );

  $product_fields = field_info_instances('uc_product');
  $image_field_options = array();
  $image_field_types = array('image', 'text', 'file');

  foreach ($product_fields as $field_instances) {
    foreach ($field_instances as $field) {
      $field_info = field_info_field($field['field_name']);
      if (in_array($field_info['type'], $image_field_types)) {
        $image_field_options[$field['field_name']] = $field['label'] . " (" . $field['field_name'] . ")";
      }
    }
  }
  if (empty($image_field_options)) {
    drupal_set_message(t('Your product needs an image field'), 'warning');
  }
  $form['uc_targetbay_image_field'] = array(
    '#type' => 'select',
    '#title' => t('Image field'),
    '#description' => t('Select the field that will be used as the image for TargetBay.'),
    '#default_value' => variable_get('uc_targetbay_image_field', 'field_images'),
    '#options' => $image_field_options,
  );

  $form = system_settings_form($form);

  if (variable_get('uc_targetbay_utoken', FALSE)) {
    $count_uc_orders = uc_targetbay_get_completed_orders(TRUE);

    if ($count_uc_orders > 0) {
      $form['actions']['sync_orders'] = array(
        '#type' => 'submit',
        '#submit' => array('uc_targetbay_settings_form_send_orders'),
        '#value' => t('Send historic orders'),
      );
    }
  }

  $form['#submit'][] = 'uc_targetbay_settings_form_token';

  $form['uc_targetbay_markup'] = array(
    '#markup' => t('Already registered? <a href="@already_registered">Click here to enter your connection information and get started!</a>',
                array('@already_registered' => url('admin/config/targetbay/connection'))),
  );
  return $form;
}

/**
 * Submit callback.
 */
function targetbay_settings_form_token($form, &$form_state) {
  if ($targetbay = uc_targetbay_new()) {
    // Retrieving the utoken - will be valid for 24 hours.
    $credentials = $targetbay->get_oauth_token();
    if (isset($credentials->access_token) && $utoken = $credentials->access_token) {
      variable_set('uc_targetbay_utoken', $utoken);
      drupal_set_message(t('TargetBay API Connected.'));
    }
  }
  else {
    variable_set('uc_targetbay_utoken', FALSE);
    form_set_error('uc_targetbay', t('There was an error getting the token. Please check your API details.'));
  }
}

/**
 * Send Orders callback.
 */
function targetbay_settings_form_send_orders($form, &$form_state) {
  // Get the uc orders available for TargetBay.
  $uc_orders = uc_targetbay_get_completed_orders();
  if (!$uc_orders) {
    drupal_set_message(t('There are no completed orders to be posted to TargetBay.'));
    return;
  }

  $response = uc_targetbay_bulk_order_post($uc_orders);

  if (isset($response->code) && $response->code == 200) {
    drupal_set_message(t('Orders posted to TargetBay.'));
  }
  else {
    drupal_set_message(t('There was an error posting orders to TargetBay.'), 'error');
  }
}

/**
 * Development -  http://dev.targetbay.com
 * Stage - https://stage.targetbay.com
 * Production - https://app.targetbay.com.
 */
function targetbay_webservice_config_form($form, &$form_state) {

  // $tzlist = DateTimeZone::targetbay_integration(DateTimeZone::ALL);.
  $module = array("Enable" => "Enable", "Disable" => "Disable");
  $api_stage = array("dev" => "Development", "stage" => "Stage", "app" => "Production");
  $page_types = array("All Pages", "Add Product", "Delete Product", "Update Product", "Page Visit", "Category View", "Product View", "Search Page", "Create Account", "Login", "Logout", "Add to cart", "Update cart", "Remove Cart", "Checkout", "Billing page", "Shipping page", "Order page", "Referrer page", "Wishlist page");
  $tracking_type = array("Automatic" => "Automatic", "Manual" => "Manual");
  $snippet = array("Disable" => "Disable", "Automatic" => "Automatic", "Manual" => "Manual");
  $tracking_script = array("tbtrack" => "Track", "tbMessage" => "Message", "tbSiteReview" => "SiteReview", "tbProductReview" => "ProductReview", "tbBulkReview" => "BulkReview", "tbQa" => "Qa");

  $form['tracking'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tracking Setup'),
    '#collapsed' => TRUE,
    '#collapsible' => FALSE,
  );

  $form['tracking']['module'] = array(
    '#type' => 'checkboxes',
    '#title' => '',
    '#description' => 'Activate this module by check this global active',
    '#options' => array('activate' => t('Global Active')),
    '#default_value' => variable_get('module', array()),
    '#attributes' => array('class' => array('tracking_module')),
  );
  $form['tracking']['hostname'] = array(
    '#type' => 'textfield',
    '#title' => 'Hostname',
    '#description' => 'Enter hostname to track (ie: example.com).',
    '#default_value' => variable_get('hostname'),
    '#attributes' => array('class' => array('tracking_hostname')),
    '#required' => TRUE,
  );
  $form['tracking']['api_token'] = array(
    '#type' => 'textfield',
    '#title' => 'Api Token',
    '#description' => '',
    '#default_value' => variable_get('api_token'),
    '#attributes' => array('class' => array('tracking_apitoken')),
    '#required' => TRUE,
  );
  $form['tracking']['api_index'] = array(
    '#type' => 'textfield',
    '#title' => 'Api Index',
    '#description' => '',
    '#default_value' => variable_get('api_index'),
    '#attributes' => array('class' => array('tracking_apiindex')),
    '#required' => TRUE,
  );
  $form['tracking']['api_stage'] = array(
    '#type' => 'select',
    '#title' => 'Api Status',
    '#description' => '',
    '#options' => $api_stage,
    '#default_value' => variable_get('api_stage'),
    '#attributes' => array('class' => array('tracking_apistage')),
    '#required' => TRUE,
  );
  $form['tracking']['page_types'] = array(
    '#type' => 'select',
    '#title' => 'Select Page Types',
    '#description' => '',
    '#options' => $page_types,
    '#multiple'     => TRUE,
    '#size'       => 9,
    '#default_value' => variable_get('page_types'),
    '#attributes' => array('class' => array('tracking_apistage')),
  );
  $form['tracking']['tracking_types'] = array(
    '#type' => 'select',
    '#title' => 'Select Tracking Type',
    '#description' => 'Full Page Cache enabled in site choose manual or choose automatic',
    '#options' => $tracking_type,
    '#default_value' => variable_get('tracking_types'),
    '#attributes' => array('class' => array('tracking_types')),
  );
  $form['tracking']['backorders'] = array(
    '#type' => 'select',
    '#title' => 'Backorders',
    '#description' => '',
    '#options' => $module,
    '#default_value' => variable_get('backorders'),
    '#attributes' => array('class' => array('tracking_backorders')),
  );
  $form['tracking']['reviews'] = array(
    '#type' => 'textfield',
    '#title' => 'Reviews per page',
    '#description' => '',
    '#default_value' => variable_get('reviews'),
    '#attributes' => array('class' => array('tracking_reviews')),
  );
  $form['tracking']['script'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Tracking Script',
    '#description' => '',
    '#options' => $tracking_script,
    '#default_value' => variable_get('script', array()),
    '#attributes' => array('class' => array('tracking_script')),
  );
  $form['tracking']['email'] = array(
    '#type' => 'select',
    '#title' => 'Disable Email:',
    '#description' => 'Yes - Disable Drupal customer welcome mail.',
    '#options' => array(t("Yes"), ("No")),
    '#default_value' => variable_get('email'),
    '#attributes' => array('class' => array('tracking_email')),
  );
  $form['tracking']['debug'] = array(
    '#type' => 'select',
    '#title' => 'Enable Debug:',
    '#description' => 'Enable or disable the Debugging.',
    '#options' => $module,
    '#default_value' => variable_get('debug'),
    '#attributes' => array('class' => array('tracking_debug')),
  );
  $form['tracking']['log_file'] = array(
    '#type' => 'textfield',
    '#title' => 'Log File Name',
    '#description' => 'Log the info',
    '#default_value' => variable_get('log_file'),
    '#attributes' => array('class' => array('tracking_logfile')),
  );

  $form['google_snippet'] = array(
    '#type' => 'fieldset',
    '#title' => t('Targetbay Google Snippet setup'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['google_snippet']['rich_snippet'] = array(
    '#type' => 'select',
    '#title' => 'Select Richsnippets Type',
    '#description' => 'Microdata alread exists in website choose manual or choose automatic',
    '#options' => $snippet,
    '#default_value' => variable_get('rich_snippet'),
    '#attributes' => array('class' => array('google_richsnippet')),
  );
  $form['google_snippet']['product_review'] = array(
    '#type' => 'select',
    '#title' => 'Enable Product Review',
    '#description' => '',
    '#options' => $module,
    '#default_value' => variable_get('product_review'),
    '#attributes' => array('class' => array('google_product_review')),
  );
  $form['google_snippet']['question_review'] = array(
    '#type' => 'select',
    '#title' => 'Enable Question Review',
    '#description' => '',
    '#options' => $module,
    '#default_value' => variable_get('question_review'),
    '#attributes' => array('class' => array('google_question_review')),
  );
  $form['google_snippet']['site_review'] = array(
    '#type' => 'select',
    '#title' => 'Enable Site Review',
    '#description' => '',
    '#options' => $module,
    '#default_value' => variable_get('site_review'),
    '#attributes' => array('class' => array('google_site_review')),
  );

  $form['google_product'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google Product Review Feed'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['google_product']['enable'] = array(
    '#type' => 'select',
    '#title' => 'Enable',
    '#description' => '',
    '#options' => $module,
    '#default_value' => variable_get('enable'),
    '#attributes' => array('class' => array('google_product_enable')),
  );

  $form['product_recommendation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Product Recommendation'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['product_recommendation']['enable_product'] = array(
    '#type' => 'select',
    '#title' => 'Enable Recommendation Products',
    '#description' => '',
    '#options' => $module,
    '#default_value' => variable_get('enable_product'),
    '#attributes' => array('class' => array('prod_recom_enable')),
  );
  $form['product_recommendation']['homepage'] = array(
    '#type' => 'textfield',
    '#title' => 'Hompage',
    '#description' => '',
    '#default_value' => variable_get('homepage'),
    '#attributes' => array('class' => array('prod_recom_homepage')),
  );
  $form['product_recommendation']['category'] = array(
    '#type' => 'textfield',
    '#title' => 'Category',
    '#description' => '',
    '#default_value' => variable_get('category'),
    '#attributes' => array('class' => array('prod_recom_category')),
  );
  $form['product_recommendation']['product'] = array(
    '#type' => 'textfield',
    '#title' => 'Product',
    '#description' => '',
    '#default_value' => variable_get('product'),
    '#attributes' => array('class' => array('prod_recom_product')),
  );
  $form['product_recommendation']['shopping_cart'] = array(
    '#type' => 'textfield',
    '#title' => 'Shopping Cart',
    '#description' => '',
    '#default_value' => variable_get('shopping_cart'),
    '#attributes' => array('class' => array('prod_recom_shopping_cart')),
  );
  $form['product_recommendation']['thank'] = array(
    '#type' => 'textfield',
    '#title' => 'Thank you',
    '#description' => '',
    '#default_value' => variable_get('thank'),
    '#attributes' => array('class' => array('prod_recom_thank')),
  );
  $form['product_recommendation']['searh'] = array(
    '#type' => 'textfield',
    '#title' => 'Search Result',
    '#description' => '',
    '#default_value' => variable_get('searh'),
    '#attributes' => array('class' => array('prod_recom_search')),
  );
  $form['product_recommendation']['404'] = array(
    '#type' => 'textfield',
    '#title' => '404 Page',
    '#description' => '',
    '#default_value' => variable_get('404'),
    '#attributes' => array('class' => array('prod_recom_404')),
  );

  $form['coupon_code'] = array(
    '#type' => 'fieldset',
    '#title' => t('Coupon Code'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['coupon_code']['product_sku'] = array(
    '#type' => 'textfield',
    '#title' => 'Product Sku',
    '#description' => '',
    '#default_value' => variable_get('product_sku'),
    '#attributes' => array('class' => array('coupon_code_productsku')),
  );

  return system_settings_form($form);
}
