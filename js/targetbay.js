(function ($) {
    Drupal.behaviors.targetbay = {
        attach: function (context, settings) {

            var username = settings.targetbay_user.user_name;
            var usermail = settings.targetbay_user.user_mail;

            if(username != '') {
            
                $('#targetbaySiteUsername').val(username);
                $('#targetbaySiteUsername').attr("disabled","disabled") 
                $('#targetbaySiteEmail').val(usermail);
                $('#targetbaySiteEmail').attr("disabled","disabled")
            }
            $('.views-field-field-course-title h1').addClass('product-name');
            
        }
    };

})(jQuery);